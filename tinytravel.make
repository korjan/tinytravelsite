api = 2

includes[] = http://drupalcode.org/project/buildkit.git/blob_plain/refs/heads/7.x-2.x:/drupal-org.make

;--------------------
; Build Kit overrides
;--------------------

projects[tao][subdir] = contrib

projects[rubik][subdir] = contrib

;--------------------
; Additional Contrib
;--------------------

projects[boxes][subdir] = contrib
projects[boxes][version] = 1.0-beta2

projects[markdown][subdir] = contrib
projects[markdown][version] = 1.0-beta1

; Creates invalid HTML with preformatted blocks.
; http://drupal.org/node/684554
projects[markdown][patch][684554] = http://drupal.org/files/issues/markdown-684554.patch

;--------------------
; Development
;--------------------

projects[coder][subdir] = contrib
projects[coder][version] = 1.0-beta6

;---------------
; Drupal basics
;--------------
projects[entity][subdir] = contrib
projects[entity][version] = 1.0-beta10
projects[token][subdir] = contrib
projects[token][version] = 1.0-beta5
projects[pathauto][subdir] = contrib
projects[pathauto][version] = 1.0-rc2
projects[libraries][subdir] = contrib
projects[libraries][version] = 1.0

;---------------
; Media
;--------------
projects[media][subdir] = contrib
projects[media][version] = 2.0-unstable1
projects[file_entity][subdir] = contrib
projects[file_entity][version] = 1.x-dev
projects[colorbox][subdir] = "contrib"
projects[colorbox][version] = 1.2

;--------------
; Location
;--------------
project[mapbox][version] = 2.0-alpha1
project[mapbox][subdir] = contrib
project[openlayers][version] = 2.0-alpha2
project[openlayers][subdir] = contrib
project[addressfield][version] = 1.0-beta2
project[addressfield][subdir] = contrib
project[geofield][version] = 1.0-alpha5
project[geofield][subdir] = contrib
project[geocoder][version] = 1.x-dev
project[geocoder][subdir] = contrib
 
;--------------
; Search
;---------------
projects[search_api][subdir] = contrib
projects[search_api][version] = 1.x-dev
projects[facetapi][subdir] = contrib
projects[facetapi][version] = 1.x-dev
projects[search_api_solr][subdir] = contrib
projects[search_api_solr][version] = 1.0-beta4
projects[search_api_autocomplete][subdir] = contrib
projects[search_api_autocomplete][version] = 1.x-dev

;-----
; Integration
;-----
projects[services][subdir] = contrib
projects[services][version] = 3.0-rc5
projects[feeds][subdir] = contrib
projects[feeds][version] = 2.0-alpha4
projects[job_scheduler][subdir] = contrib
projects[job_scheduler][version] = 2.0-alpha2

;------------------
; UI
;------------------
projects[omega][subdir] = contrib
projects[omega][version] = 3.0
projects[omega_tools][subdir] = contrib
projects[omega_tools][version] = 3.0-rc3
projects[delta][subdir] = contrib
projects[delta][version] = 3.0-beta8
projects[fontyourface][subdir] = contrib
projects[fontyourface][version] = 1.6

;--------
; Libraries
;----------

libraries[colorbox][download][type] = get
libraries[colorbox][download][url] = http://colorpowered.com/colorbox/latest
libraries[colorbox][directory_name] = colorbox

libraries[SolrPhpClient][download][type] = get
libraries[SolrPhpClient][download][url] = http://solr-php-client.googlecode.com/files/SolrPhpClient.r22.2009-11-09.tgz
libraries[SolrPhpClient][directory_name] = SolrPhpClient
libraries[SolrPhpClient][destination] = modules/contrib/search_api_solr

libraries[geoPHP][download][type] = get
libraries[geoPHP][download][url] = https://github.com/downloads/phayes/geoPHP/geoPHP.tar.gz
libraries[geoPHP][directory_name] = geoPHP
libraries[geoPHP][destination] = geoPHP

;-----------
; Custom
;----------
projects[tinytravelomega][type] = theme
projects[tinytravelomega][subdir] = custom
projects[tinytravelomega][download][type] = git
projects[tinytravelomega][download][url] = git://github.com/korjan/tinytravelomega.git

